#!/bin/bash
set -e

pkgs=("python-mouseinfo" "python-pymsgbox" "python-pytweening" "python-pyscreeze" "python-pyautogui" "gconf" "lib32-gconf")

# use all cores
sudo sed -i 's/#MAKEFLAGS="-j2"/MAKEFLAGS="-j$(nproc)"/g' /etc/makepkg.conf

mkdir ~/pkgs
cd ~/
for pkg in "${pkgs[@]}"; do
        git clone https://aur.archlinux.org/"$pkg".git
        cd "$pkg"
        makepkg -sic --noconfirm
        cp -v *.pkg.tar.* ~/pkgs/
        cd ~/
done
